Feature: NBA Drupal Admin Page
  In order to work on the NBA Admin page
  As a editor
  I need to be able to use NBA Admin page powered by Drupal 8

  Scenario:
    Given I go to "http://www.nba.com"
    When I see the "NBA" logo
    Then I see the "Games" link
    And I see the "Top Stories" link
    And I see the "Video" link
    And I see the "Standings" link
    And I see the "Stats" link
    And I see the "Players" link
    And I see the "Teams" link
    And I close the web browser
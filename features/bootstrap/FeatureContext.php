<?php

/**
 *
 * Copyright (c) 2016 by Turner Broadcasting System, Inc.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Turner Broadcasting System,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Turner Broadcasting System.
 *
 * @Project: Marty
 * @Author:  hnguyen
 * @Version: 1.0
 * @Description:  php script opens the NBA.com site
 *
 * @Date created: April 13, 2016
 *
 */

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;

use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Session;
use Behat\Mink\Selector\Xpath\Escaper;

require __DIR__ . '\..\..\vendor\autoload.php';
require __DIR__ . '\..\Helpers\SearchHelper.php';
require __DIR__ . '\..\Helpers\ClickHelper.php';
//require __DIR__ . '\..\Helpers\DataEntryHelper.php';
//require __DIR__ . '\..\Helpers\UploadHelper.php';

//require_once('SearchHelper.php');

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends PHPUnit_Framework_TestCase implements Context, SnippetAcceptingContext
{
    private $driver;
    private $session;
    private $xpathEscaper;
    private $file;
    static $logFile;
    private $handle;
    private $currentDate;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        global $logFile;

        parent::__construct();
        $this->driver = new Selenium2Driver('chrome');
        $this->session = new Session($this->driver);
        $this->xpathEscaper = new Escaper();
        $this->file = $logFile;

    }

    /**
     * @BeforeSuite
     */
    public static function startSeleniumServer()
    {

        global $logFile;

        // martyLog.txt is a log file for troubleshooting the script
        // if there is something wrong.  The file path myLogFile is
        // defined in config.ini file.
        $ini_array = parse_ini_file('/../config.ini');

        $logFile        = $ini_array['myLogFile'];
        $SeleniumServer = $ini_array['mySeleniumServer'];
        $ChromeDriver   = $ini_array['myChromeDriver'];

        file_put_contents($logFile, LOCK_EX);

        $handle = fopen($logFile,'a') or die('Cannot open file:  '.$logFile);

        date_default_timezone_set('America/New_York');

        // starts the selenium server in Windows.  If this test suite is run
        // in other OS, COM cannot be used.
        $WshShell = new COM("WScript.Shell");

        //$oExec = $WshShell->Run('java -jar "C:\Users\hnguyen\Downloads\selenium-server-standalone-2.53.0.jar" -Dwebdriver.firefox.bin="C:\Program Files\Mozilla Firefox\firefox.exe"', 10, false);
        //$oExec = $WshShell->Run("java -jar C:\Users\hnguyen\Downloads\selenium-server-standalone-2.53.0.jar -Dwebdriver.chrome.driver=C:\Users\hnguyen\Downloads\chromedriver_win32\chromedriver.exe", 10, false);
        $oExec = $WshShell->Run("java -jar $SeleniumServer -Dwebdriver.chrome.driver=$ChromeDriver");

        $currentDate = date('Y-m-d H:i:s');
        sleep(3);       // slow down so that the selenium can start completely

        if ($oExec == 0)
            $current = "\r\n" .$currentDate. " startSeleniumServer():: " . $oExec . "\r\n";
        else
            $current = "\r\n" .$currentDate. " startSeleniumServer():: Cannot start the selenium server.\r\n";

        // Write to the log file
        file_put_contents($logFile, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @Given I go to :aHomePage
     *
     * This function opens a web site.  In Mink, the entry point
     * to the browser is called the session
     *
     * @param $aHomePage
     *
     */
    public function openWebSite($aHomePage)
    {
        // start the session and open the web site
        $this->session->start();
        $this->session->visit($aHomePage);

        // to verify the status of visit, can call a javascript and
        // check its status as follows:
        //$this->session->evaluateScript("aJavaScript");

        // Open the file to get existing content
        //$current = file_get_contents($this->file);
        $this->handle = fopen($this->file,'a') or die('Cannot open file:  '.$this->file);
        //error_log($this->handle, 3, 'C:\errorLog.txt');

        date_default_timezone_set('America/New_York');
        $this->currentDate = date('Y-m-d H:i:s');

        // note that selenium2 does not provide the http status code.
        // The only way to verify that a page can be opened is to check
        // if something is found on that page, eg. Log In link
        $current = "\r\n" .$this->currentDate. " openWebSite()::URL: " . $this->session->getCurrentUrl() . "\r\n";

        // Write to the log file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
    }

    /**
     * @When I see the :aLink link/logo
     *
     * This function looks up a link
     *
     * @param $aLink
     *
     */
    public function locateTheLink($aLink)
    {

        // get the page document
        $page = $this->session->getPage();

        // search the page document for a  specific link
        $linkObj = searchLink($page, $aLink);

        $this->currentDate = date('Y-m-d H:i:s');

        if (is_null($linkObj))
            $current = "\r\n" .$this->currentDate. " locateTheLink()::The link '" .$aLink. "' cannot be found.";
        else
            $current = "\r\n" .$this->currentDate. " locateTheLink()::The link '" .$linkObj->getText(). "' is found.";

        // Write to the file
        file_put_contents($this->file, $current, FILE_APPEND | LOCK_EX);
        sleep(1);
    }

    /**
     * @When I close the web browser
     */
    public function closeTheWebBrowser()
    {
        file_put_contents($this->file, "\r\n--------------------------------------", FILE_APPEND | LOCK_EX);
        // close the log file
        fclose($this->handle);
        $this->session->stop();
    }

    /**
     * @AfterSuite
     */
    public static function stopSeleniumServer()
    {
        //$myCmd = "FOR /F \"tokens=5 delims= \" %P IN ('netstat -a -n -o ^| findstr :4444') DO TaskKill.exe /F /PID %P";
        //pclose(popen("$myCmd 2>nul >nul", "r"));

        $WshShell = new COM("WScript.Shell");
        $WshShell->Run("Taskkill /F /IM java.exe", 10, false);
        //$WshShell->Run("Taskkill /F /IM cmd.exe", 10, false);
    }

}
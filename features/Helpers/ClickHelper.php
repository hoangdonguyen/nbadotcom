<?php
/**
 * Created by PhpStorm.
 * User: hnguyen
 * Date: 4/6/2016
 * Time: 2:31 PM
 */

function clickALink($page, $aLink)
{
    $page->findLink($aLink)->click();
    sleep(1);   // slow down so that we can see the new page
}

function pressAButton($page, $aButton)
{
    // button can also be found by using css selector
    // $page->find('css','input[id="edit-submit"]')->click();
    
    $page->findButton($aButton)->press();
}

function clickADropDownMenu($page, $aHref)
{
    $dropDownMenu = $page->find('xpath', "//a[@href='$aHref']");
    return $dropDownMenu;
}

function clickAToolBarIcon($page)
{
    
}
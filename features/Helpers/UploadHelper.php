<?php
/**
 * Created by PhpStorm.
 * User: hnguyen
 * Date: 4/7/2016
 * Time: 4:12 PM
 */
/*
 *
 * This function upload an img
 *
 */
function uploadThing($page, $imgLoc)
{
    $page->attachFileToField("edit-icon-upload-upload", $imgLoc);
    sleep(5);   // slow down so that img canbe uploaded completely.  Perhaps a
                // polling loop should be implemented to check when the upload
                // is done
}
